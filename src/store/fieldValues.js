import { writable } from 'svelte/store';

const initialState = {
  email: 'morty.smith@pm.me'
};

const { subscribe, update, set } = writable(initialState);

// prettier-ignore
const setField = (key, value) => update(fieldValues => ({
  ...fieldValues,
  [key]: value
}));

const resetField = () => set(initialState);

export default {
  subscribe,
  setField,
  resetField
};
