import { writable } from 'svelte/store';

const initialState = 0;

const { subscribe, update } = writable(initialState);

const incrementIndex = () => update(currentIdx => currentIdx + 1);

const decrementIndex = () => update(currentIdx => currentIdx - 1);

export default {
  subscribe,
  incrementIndex,
  decrementIndex
};
